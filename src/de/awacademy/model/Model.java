package de.awacademy.model;

import de.awacademy.objectsgiven.*;
import de.awacademy.stages.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.stream.Collectors;

public class Model {

    private Player player;
    public final double WIDTH = 800;
    public final double HEIGHT = 800;
    private HashSet<Walls> gameBorderWalls;
    private ArrayList<Tanks> enemyTanks;
    private ArrayList<Bullets> bulletsinGame;

    private ArrayList<Walls> otherwalls;

    private ArrayList<Stages> stages;

    private int stageindex;

    private int pointCounter;

    private boolean gameOver;
    private boolean win;


    public Model() {
        this.player = new Player((int) WIDTH*1/2, (int) HEIGHT*7/8);
        player.setX(WIDTH/2 - player.getWidth()/2);
        player.setY((HEIGHT*7)/8-player.getLength()/2);
        player.getShootingBarrel().setX(player.getX()+20);
        player.getShootingBarrel().setY(player.getY()+20);
        player.getShootingBarrel().move();
        pointCounter=0;

        gameBorderWalls = new HashSet<>();
        gameBorderWalls.add(new Walls(0,(int) HEIGHT -20,(int) WIDTH ,20));//10 up
        gameBorderWalls.add(new Walls(0,0,(int) WIDTH ,20)); //10 down
        gameBorderWalls.add(new Walls((int) WIDTH -20,0,20,(int) HEIGHT));// 10 left
        gameBorderWalls.add(new Walls(0,0 ,20,(int) HEIGHT));//10 right

        stages = new ArrayList<>();
        stageindex = 0;
        enemyTanks = new ArrayList<>();
        otherwalls = new ArrayList<>();
        //enemyTanks.add(new MovingTank((int) WIDTH*1/4, (int) HEIGHT*1/4))

        bulletsinGame= new ArrayList<>();
        stages.add(new Stage1(WIDTH,HEIGHT));
        stages.add(new Stage2(WIDTH,HEIGHT));
        stages.add(new Stage3(WIDTH,HEIGHT));
        stages.add(new Stage4(WIDTH,HEIGHT));
        stages.add(new Stage5(WIDTH,HEIGHT));
        stages.add(new Stage6(WIDTH,HEIGHT));
        stages.add(new WinningStage(WIDTH,HEIGHT));
        stages.add(new GameOver(WIDTH,HEIGHT));
        enemyTanks = stages.get(stageindex).getEnemytanks();
        otherwalls = stages.get(stageindex).getWalls();

        gameOver=false;
        win=false;
    }

    public boolean collisionDetection(CollidleObjects object1, CollidleObjects object2){
        boolean condition1 = object1.getX() < object2.getX() + object2.getWidth();
        boolean condition2 = object1.getY() < object2.getY() + object2.getLength();
        boolean condition3 = object1.getX() + object1.getWidth() > object2.getX();
        boolean condition4 = object1.getY() + object1.getLength() > object2.getY();

        if (condition1 && condition2 && condition3 && condition4) {
            return true;
        }
        return false;
    }

    public boolean collisionDetection(CollidleObjects object1, CollidleObjects object2, double object1dx, double object1dy){
        boolean condition1 = object1.getX() + object1dx < object2.getX() + object2.getWidth();
        boolean condition2 = object1.getY() + object1dy< object2.getY() + object2.getLength();
        boolean condition3 = object1.getX() + object1dx + object1.getWidth() > object2.getX();
        boolean condition4 = object1.getY() + object1dy + object1.getLength() > object2.getY();

        if (condition1 && condition2 && condition3 && condition4) {
            return true;
        }
        return false;
    }

    public int collisionDetectionWall(CollidleObjects object1, CollidleObjects object2, double object1dx, double object1dy){
        boolean condition1 = object1.getX() + object1dx < object2.getX() + object2.getWidth();
        boolean condition2 = object1.getY() < object2.getY() + object2.getLength();
        boolean condition3 = object1.getX() + object1dx + object1.getWidth() > object2.getX();
        boolean condition4 = object1.getY() + object1.getLength() > object2.getY();//remove dy to test dx

        boolean condition5 = object1.getX() < object2.getX() + object2.getWidth();
        boolean condition6 = object1.getY() + object1dy< object2.getY() + object2.getLength();
        boolean condition7 = object1.getX() + object1.getWidth() > object2.getX();
        boolean condition8 = object1.getY() + object1dy + object1.getLength() > object2.getY();//remove dx to test dy

        if(condition1&&condition2&&condition3&&condition4&&condition5&&condition6&&condition7&&condition8){//corner
            return 3;
        }else if(condition1&&condition2&&condition3&&condition4){//dy removed so x side collision
            return 2;
        }else if(condition5&&condition6&&condition7&&condition8){
            return 1;
        }
        return 0;
    }

    public void isHit() {
        ArrayList<Bullets> bullets = new ArrayList<>(bulletsinGame);
        for (Bullets bullet : bulletsinGame) {
            bullets.remove(bullet);
            if (bullet.isIngame()) {
                for (Walls wall : gameBorderWalls) {
                    int number = collisionDetectionWall(bullet,wall,bullet.getDx(),bullet.getDy());
                    if (number>0) {
                        bullet.hitwall(number);
                        continue;
                    }
                }

                if(otherwalls.size()>0&&!bullet.isGhost()){
                    for (Walls wall: otherwalls) {
                        int number = collisionDetectionWall(bullet, wall, bullet.getDx(), bullet.getDy());
                        if(!wall.isDestructable()){

                            if (number > 0) {
                                bullet.hitwall(number);
                                continue;
                            }
                        }else{
                            if (number > 0) {
                                bullet.setIngame(false);
                                wall.setDestroyed(true);
                            }
                        }
                    }
                }


            }
            if (bullet.isIngame()) {
                for (Bullets bullet2 : bullets) {

                    if (collisionDetection(bullet,bullet2)) {
                        bullet.setIngame(false);
                        bullet2.setIngame(false);
                        break;
                    }
                }

            }
            if (bullet.isIngame()) {

                if (collisionDetection(bullet,player)&&player.isAlive()) {
                    player.setAlive(false);
                    bullet.setIngame(false);
                    continue;
                }
            }


            if (bullet.isIngame()) {

                for (Tanks tank : enemyTanks) {

                    if (collisionDetection(bullet,tank)&&tank.isAlive()) {
                        tank.setAlive(false);
                        bullet.setIngame(false);

                        pointCounter++;

                        continue;

                    }
                }
            }

        }

    }

    public boolean isoccupied(Tanks tankinquestion,int dx, int dy){
        // checks if space is occupied by anything else other than the player

        for (Walls wall: gameBorderWalls) {
            if(collisionDetection(tankinquestion,wall,dx,dy)){
                return true;
            }
        }
        for (Walls wall: otherwalls) {
            if(collisionDetection(tankinquestion,wall,dx,dy)){
                return true;
            }
        }

        if(tankinquestion!=this.player){

            if (collisionDetection(tankinquestion,player,dx,dy)) {
                return true;
            }
        }


        for (Tanks tank:enemyTanks) {
            if (tank != tankinquestion) {

                if (collisionDetection(tankinquestion,tank,dx,dy) ){
                    return true;
                }

            }
        }

        return false;
    }

    public void shootbyTank(Tanks tank){
        if(tank.getTanksBullets().size()<tank.getBulletlimit()) {
            tank.getShootingBarrel().makemove(player.getX(), player.getY());//moves Barrel, very important for teh first move!!!! otherwise kills itself
            Bullets bulletshot = tank.getShootingBarrel().shoot();
            bulletsinGame.add(bulletshot);
            tank.getTanksBullets().add(bulletshot);
        }
    }

    public void shootbyPLayer(int mag){
        if(player.getTanksBullets().size()<player.getBulletlimit()) {
            Bullets bulletshot = player.getShootingBarrel().shoot();
            bulletsinGame.add(bulletshot);
            player.getTanksBullets().add(bulletshot);
            enemyTanks.forEach(s -> s.setBulletAllert(true));
        }

    }

    //Getter
    public Player getPlayer() {
        return player;
    }

    public HashSet<Walls> getGameBorderWalls() {
        return gameBorderWalls;
    }

    public ArrayList<Tanks> getEnemyTanks() {
        return enemyTanks;
    }

    public ArrayList<Bullets> getBulletsinGame() {
        return bulletsinGame;
    }

    public int getPointCounter() {
        return pointCounter;
    }

    public ArrayList<Walls> getOtherwalls() {
        return otherwalls;
    }

    public int getStageindex() {
        return stageindex;
    }

    public ArrayList<Stages> getStages() {
        return stages;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setStageindex(int stageindex) {
        this.stageindex = stageindex;
    }

    public boolean isWin() {
        return win;
    }

    public void enemyMovement(Tanks tank, double distancex, double distancey){
        int speed = tank.getSpeed();
        if (Math.abs(distancex) > Math.abs(distancey)) {
            if (Math.abs(distancex - distancey) < 20) {
                if (distancey > 0) {
                    tank.setDy(speed/2+1);
                }if(distancey<0){
                    tank.setDy(-speed/2+1);
                }if(distancex>0){
                    tank.setDx(speed/2);
                }if(distancex<0){
                    tank.setDx(-speed/2);
                }
            }else {
                if (distancey > 0) {
                    tank.setDy(speed);
                } else {
                    tank.setDy(-speed);
                }
            }
        }else{
            if (Math.abs(distancex - distancey) < 20) {
                if (distancex > 0) {
                    tank.setDx(speed/2+1);
                }if(distancex<0){
                    tank.setDx(-speed/2+1);
                }if(distancey>0){
                    tank.setDy(speed/2);
                }if(distancey<0){
                    tank.setDy(-speed/2);
                }
            }else {
                if (distancex > 0) {
                    tank.setDx(speed);
                } else {
                    tank.setDx(-speed);
                }
            }

        }
    }

    public void makenewStage(){
        bulletsinGame.clear();
        player.getTanksBullets().clear();//otherwise all bullets that were left in the old stage would be lost to the player
        enemyTanks = stages.get(stageindex).getEnemytanks();
        otherwalls = stages.get(stageindex).getWalls();
        player.setX(WIDTH/2 - player.getWidth()/2);
        player.setY((HEIGHT*7)/8-player.getLength()/2);
        player.getShootingBarrel().setX(player.getX()+20);
        player.getShootingBarrel().setY(player.getY()+20);
        player.getShootingBarrel().move();
        if(stageindex==stages.size()-2){//needed as game over condition requires player to be dead
            for (Stages stagegiven: stages) {
                win=true;
                //if(stagegiven.getEnemytanks().size()!=0) {
                //  stagegiven.getEnemytanks().clear();
                //}
                stagegiven.createStage();
            }
            stageindex=-1;
        }
    }

    public void moveawayFromWalls(Tanks tank, Walls wall){
        double dx1 = tank.getX() - wall.getX();
        double dy1 = tank.getY() - wall.getY();
        int signx= 1;
        int signy=1;

        if(dx1>0){
            dx1-=wall.getWidth();//if dx1 is positive it must be subtracted with the width of the wall
            signx=-1;
        }else{
            dx1+=tank.getWidth();
        }
        if(dy1>0){
            dy1-=wall.getLength();
            signy=-1;
        }else{
            dy1+=tank.getLength();
        }

        if(collisionDetection(tank,wall,15*signx,15*signy)) {
            enemyMovement(tank,dx1,dy1);
        }
    }

    public void update(long nowNano) {
        if(enemyTanks.size()!=0) {

            for (Tanks tank : enemyTanks) {
                if(tank.isBulletAllert()) {
                    double distancex = tank.getX() - player.getX();//distx neg => player right of tank
                    double distancey = tank.getY() - player.getY();//disty nef => player below tank
                    enemyMovement(tank,distancex,distancey);

                    for (Walls wall: otherwalls) {
                        moveawayFromWalls(tank,wall);
                    }
                    for (Walls wall: gameBorderWalls) {
                        moveawayFromWalls(tank,wall);
                    }



                }else{
                    tank.setDx(0);
                    tank.setDy(0);
                }



                if(nowNano%1000==0){
                if (!this.isoccupied(tank, tank.getDx(), tank.getDy())) {
                    tank.move(tank.getDx(), tank.getDy());
                }

                }
                if((int)(Math.random()*100)==10){
                    tank.setBulletAllert(false);
                }

//                if(nowNano%20000==0){
//                    enemyTanks.forEach(s->s.setBulletAllert(false));
//                }

                //if(nowNano%10000==0){
                //    shootbyTank(tank);
                //}
                if((int)(Math.random()*50)==10){
                    shootbyTank(tank);
                }


            }

       for (Bullets bullet:bulletsinGame) {

            if(bullet.isIngame()) {
                bullet.move();
            }
        }
        isHit();
        bulletsinGame.removeIf(s -> !s.isIngame());
        player.getTanksBullets().removeIf(t->!t.isIngame());
        enemyTanks.forEach(s -> s.getTanksBullets().removeIf(t->!t.isIngame()));
        enemyTanks.removeIf(s -> !s.isAlive());
        otherwalls.removeIf(s ->s.isDestroyed());
        if(!player.isAlive()){
            stageindex=stages.size()-1;
            bulletsinGame.clear();
            enemyTanks.clear();
            player.getTanksBullets().clear();
            enemyTanks = stages.get(stageindex).getEnemytanks();
            otherwalls = stages.get(stageindex).getWalls();
            player.setAlive(true);

            player.setX(WIDTH/2 - player.getWidth()/2);
            player.setY((HEIGHT*7)/8-player.getLength()/2);
            player.getShootingBarrel().setX(player.getX()+20);
            player.getShootingBarrel().setY(player.getY()+20);
            player.getShootingBarrel().move();
            gameOver=true;

            for (Stages stagegiven: stages) {
                //if(stages.get(stageindex)!=stagegiven) {
                if(stagegiven.getEnemytanks().size()!=0) {
                    stagegiven.getEnemytanks().clear();
                }
                stagegiven.createStage();

                //}
            }
            stageindex=-1;
        }
        if(enemyTanks.size()==0) {

            win=false;
            gameOver=false;
            stageindex++;
            makenewStage();
            /*bulletsinGame.clear();
            player.getTanksBullets().clear();//otherwise all bullets that were left in the old stage would be lost to the player
            enemyTanks = stages.get(stageindex).getEnemytanks();
            otherwalls = stages.get(stageindex).getWalls();
            player.setX(WIDTH/2);
            player.setY((HEIGHT*7)/8);
            player.getShootingBarrel().setX(player.getX()+20);
            player.getShootingBarrel().setY(player.getY()+20);
            player.getShootingBarrel().move();*/

            /*if(stageindex==stages.size()-2){//needed as game over condition requires player to be dead
                for (Stages stagegiven: stages) {
                    win=true;
                    //if(stagegiven.getEnemytanks().size()!=0) {
                      //  stagegiven.getEnemytanks().clear();
                    //}
                    stagegiven.createStage();
                }
                stageindex=-1;
            }*/

        }
        }
    }
}
