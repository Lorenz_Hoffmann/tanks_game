package de.awacademy.model;

import de.awacademy.objectsgiven.BouncingBullet;
import de.awacademy.objectsgiven.NormalBullet;
import de.awacademy.objectsgiven.ShootingBarrel;
import de.awacademy.objectsgiven.Tanks;

public class Player extends Tanks {


    public Player(int x, int y) {
        super(x,y);
        setShootingBarrel(new ShootingBarrel(x+20,y+20,new BouncingBullet()));
    }

}
