package de.awacademy.model;

public interface CollidleObjects {
    double getX();
    double getY();
    int getLength();
    int getWidth();


}
