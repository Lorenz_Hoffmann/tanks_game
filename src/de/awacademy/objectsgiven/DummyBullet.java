package de.awacademy.objectsgiven;

public class DummyBullet extends Bullets {

    public DummyBullet() {
        setMag(2);
        setIngame(false);
    }

    public DummyBullet(double x, double y, double xdir, double ydir){
        super(x,y,xdir,ydir);
        setMag(2);
        setDx(getMag()*xdir);
        setDy(getMag()*ydir);
        setNumberBounces(1);
        setIngame(false);
    }
}
