package de.awacademy.objectsgiven;

public class FastBullet extends Bullets{

    public FastBullet() {
        setMag(5);
    }

    public FastBullet(double x, double y, double xdir, double ydir){
        super(x,y,xdir,ydir);
        setMag(5);
        setDx(getMag()*xdir);
        setDy(getMag()*ydir);
        setNumberBounces(1);
    }
}
