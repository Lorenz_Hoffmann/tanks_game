package de.awacademy.objectsgiven;

public class DummyTank extends Tanks {
    public DummyTank(int x, int y) {
        super(x, y);
        setShootingBarrel(new ShootingBarrel(x+20,y+20,new DummyBullet()));
    }
    @Override
    public void move(int dx, int dy) {

    }


}
