package de.awacademy.objectsgiven;

public class GhostTank extends Tanks{
    public GhostTank(int x, int y) {
        super(x, y);
        setShootingBarrel(new ShootingBarrel(x+20,y+20,new GhostBullet()));
    }
}
