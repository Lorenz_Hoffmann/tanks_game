package de.awacademy.objectsgiven;

import de.awacademy.model.CollidleObjects;

public abstract class Bullets implements CollidleObjects {

    private double x;
    private double y;
    private int numberBounces;

    private boolean ghost;

    private int width;
    private int height;

    private int mag;
    private boolean ingame;
    private double xdir;
    private double ydir;
    public Bullets(){}

    private double dx;
    private double dy;

    public Bullets(double x, double y, double xdir, double ydir) {
        this.x = x;
        this.y = y;
        this.xdir=xdir;
        this.ydir=ydir;

        this.width=10;
        this.height=10;
        this.ingame=true;


    }

    public void move(){
        this.x = x+dx;
        this.y = y+dy;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getXdir() {
        return xdir;
    }

    public double getYdir() {
        return ydir;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return height;
    }

    public double getDx() {
        return dx;
    }

    public double getDy() {
        return dy;
    }

    public boolean isIngame() {
        return ingame;
    }

    public int getMag() {
        return mag;
    }

    public boolean isGhost() {
        return ghost;
    }

    public void setGhost(boolean ghost) {
        this.ghost = ghost;
    }

    public void setIngame(boolean ingame) {
        this.ingame = ingame;
    }

    public void hitwall(int number){
        if(number==3){
            dx = -getDx();
            dy = -getDy();
        }if (number==2){
            dx = -getDx();
        }if (number==1){
            dy = -getDy();
        }

        numberBounces--;


        if(numberBounces==0){
            setIngame(false);

        }

    }

    public void setMag(int mag) {
        this.mag = mag;
    }

    public void setNumberBounces(int numberBounces) {
        this.numberBounces = numberBounces;
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public void setDy(double dy) {
        this.dy = dy;
    }
}
