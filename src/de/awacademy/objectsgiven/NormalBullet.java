package de.awacademy.objectsgiven;

public class NormalBullet extends Bullets{

    public NormalBullet() {
        setMag(2);
    }

    public NormalBullet(double x, double y, double xdir, double ydir){
       super(x,y,xdir,ydir);
       setMag(2);
       setDx(getMag()*xdir);
       setDy(getMag()*ydir);
       setNumberBounces(1);
   }

}
