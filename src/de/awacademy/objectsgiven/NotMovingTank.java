package de.awacademy.objectsgiven;

public class NotMovingTank extends Tanks {
    public NotMovingTank(int x, int y) {
        super(x, y);
        setShootingBarrel(new ShootingBarrel(x+20,y+20,new BouncingBullet()));
    }

    @Override
    public void move(int dx, int dy) {

    }
}
