package de.awacademy.objectsgiven;

public class MovingTank extends Tanks{
    public MovingTank(int x, int y) {
        super(x, y);
        setShootingBarrel(new ShootingBarrel(x+20,y+20,new NormalBullet()));
    }
}
