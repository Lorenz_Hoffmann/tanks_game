package de.awacademy.objectsgiven;

public class GhostBullet extends Bullets{

    public GhostBullet() {
        setMag(2);
    }

    public GhostBullet(double x, double y, double xdir, double ydir){
        super(x,y,xdir,ydir);
        setMag(2);
        setDx(getMag()*xdir);
        setDy(getMag()*ydir);
        setNumberBounces(1);
        setGhost(true);
    }
}
