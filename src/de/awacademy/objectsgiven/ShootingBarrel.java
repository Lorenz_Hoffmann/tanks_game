package de.awacademy.objectsgiven;

public class ShootingBarrel {
    private double x; //xpos for middle of tank
    private double y;
    private double width;
    private double length;

    private double dirOfx;
    private double dirOfy;

    private double truex;//actual xpos
    private double truey;

    private double lengthfordrawing;
    private double widthfordrawing;

    private double endofbarrelx;
    private double endofbarrely;

    private double angleToCenter;


    private Bullets bullettype;

    public ShootingBarrel(int x, int y, Bullets bullettype) {
        this.x = x;
        this.y = y;

        this.bullettype = bullettype;
        this.length=40;
        this.width=10;

        this.lengthfordrawing=length;
        this.widthfordrawing=width;

        this.truex=x;
        this.truey=y;

        this.endofbarrelx=x;
        this.endofbarrely= y+length;
        this.angleToCenter=0;
    }


    public void makemove(double dirx, double diry){
        double ankathete= dirx -x;
        double gegenkathete= diry -y;

        double angle = Math.atan(gegenkathete/ankathete);
        if(ankathete>0&&gegenkathete<0){//a = pos
            this.angleToCenter=((angle*180/Math.PI-90));
        }else if(ankathete<0&&gegenkathete>0){//g = pos
            this.angleToCenter=((90+angle*180/Math.PI));
        }else if(ankathete>0&&gegenkathete>0){//both pos
            this.angleToCenter=((angle*180/Math.PI-90));
        }else if(ankathete<0&&gegenkathete<0){//both neg
            this.angleToCenter=((90+angle*180/Math.PI));
        }

        double cosineornomx= Math.cos(angle);
        double sinusornormy= Math.sin(angle);
        if (ankathete<0){
            this.dirOfx=-Math.abs(cosineornomx);
        }
        if (ankathete>=0){
            this.dirOfx=Math.abs(cosineornomx);
        }
        if (gegenkathete<0){
            this.dirOfy=-Math.abs(sinusornormy);
        }
        if (gegenkathete>=0){
            this.dirOfy=Math.abs(sinusornormy);
        }



        move();
    }

    public void move(){

            this.truex = x;
            this.truey = y;
            this.endofbarrelx = x + length * dirOfx;
            this.endofbarrely = y + length * dirOfy;

            this.lengthfordrawing = length;
            this.widthfordrawing = width;
        //if(dirOfx ==0&&dirOfy==1) {
            /*
            //down
        }else if(dirOfx ==1&&dirOfy==0){
            this.truex=x;
            this.truey=y;
            this.endofbarrelx=x+length;
            this.endofbarrely=y;

            this.lengthfordrawing=width;
            this.widthfordrawing=length;

            //right
        }else if(dirOfx ==-1&&dirOfy==0){
            this.truex=x-length;
            this.truey=y;
            this.endofbarrelx=x-length;
            this.endofbarrely=y;

            this.lengthfordrawing=width;
            this.widthfordrawing=length;

            //left
        }else if(dirOfx ==0&&dirOfy==-1){
            this.truex=x;
            this.truey=y -length;
            this.endofbarrelx=x;
            this.endofbarrely=y-length;

            this.lengthfordrawing=length;
            this.widthfordrawing=width;
            //up
        }

             */
    }

    public Bullets shoot() {

        if (bullettype instanceof NormalBullet) {
            return new NormalBullet(endofbarrelx, endofbarrely, dirOfx, dirOfy);//dirofx gives value between 0 and 1
        } else if(bullettype instanceof BouncingBullet){
            return new BouncingBullet(endofbarrelx, endofbarrely, dirOfx, dirOfy);
        } else if(bullettype instanceof DummyBullet){
            return new DummyBullet(endofbarrelx, endofbarrely, dirOfx, dirOfy);
        } else if(bullettype instanceof FastBullet){
            return new FastBullet(endofbarrelx, endofbarrely, dirOfx, dirOfy);
        } else if(bullettype instanceof GhostBullet){
            return new GhostBullet(endofbarrelx, endofbarrely, dirOfx, dirOfy);
        }

        else {
                return new NormalBullet();
        }
    }


    //Getter


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getTruex() {
        return truex;
    }

    public double getTruey() {
        return truey;
    }

    public double getLengthfordrawing() {
        return lengthfordrawing;
    }

    public double getWidthfordrawing() {
        return widthfordrawing;
    }

    public Bullets getBullettype() {
        return bullettype;
    }

    public double getEndofbarrelx() {
        return endofbarrelx;
    }

    public double getEndofbarrely() {
        return endofbarrely;
    }

    public double getAngleToCenter() {
        return angleToCenter;
    }

    //Setter


    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
