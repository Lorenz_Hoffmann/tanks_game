package de.awacademy.objectsgiven;

public class BouncingBullet extends Bullets {



    public BouncingBullet() {
        setMag(2);
    }

    public BouncingBullet(double x, double y, double xdir, double ydir){
        super(x,y,xdir,ydir);
        setMag(2);
        setDx(getMag()*xdir);
        setDy(getMag()*ydir);
        setNumberBounces(2);

    }
}
