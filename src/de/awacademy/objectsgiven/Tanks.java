package de.awacademy.objectsgiven;

import de.awacademy.model.CollidleObjects;

import java.util.ArrayList;

public abstract class Tanks implements CollidleObjects {

    private ShootingBarrel shootingBarrel;

    private double x;
    private double y;

    private int dx;
    private int dy;

    private boolean alive;
    private int width;
    private int length;

    private int bulletlimit;

    private ArrayList<Bullets> tanksBullets;

    private boolean bulletAllert;

    private int speed;

    public Tanks(){

    }

    public Tanks(int x, int y) {
        this.x = x;
        this.y = y;

        this.dx = 0;
        this.dy = 0;

        this.alive=true;
        this.width=40;
        this.length=40;
        this.bulletlimit=5;
        this.tanksBullets= new ArrayList<>();
        this.speed=5;

    }

    public void move(int dx, int dy){
        this.x += dx;
        this.y += dy;
        this.shootingBarrel.setX(shootingBarrel.getX()+dx);
        this.shootingBarrel.setY(shootingBarrel.getY()+dy);
        this.shootingBarrel.move();
    }


    //Getter

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isBulletAllert() {
        return bulletAllert;
    }

    public int getSpeed() {
        return speed;
    }

    public int getBulletlimit() {
        return bulletlimit;
    }

    public ArrayList<Bullets> getTanksBullets() {
        return tanksBullets;
    }

    public ShootingBarrel getShootingBarrel() {
        return shootingBarrel;
    }
    //Setter

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setBulletAllert(boolean bulletAllert) {
        this.bulletAllert = bulletAllert;
    }

    public void setShootingBarrel(ShootingBarrel shootingBarrel) {
        this.shootingBarrel = shootingBarrel;
    }

    public void setBulletlimit(int bulletlimit) {
        this.bulletlimit = bulletlimit;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
