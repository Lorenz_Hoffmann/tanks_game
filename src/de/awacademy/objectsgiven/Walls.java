package de.awacademy.objectsgiven;

import de.awacademy.model.CollidleObjects;

public class Walls implements CollidleObjects {
    private boolean destructable;
    private double x;
    private double y;
    private int length;
    private int width;
    private boolean destroyed;



    public Walls() {
    }

    public Walls(int x, int y, int width, int length) {
        this.x = x;
        this.y = y;
        this.length = length;
        this.width = width;
        this.destructable=false;
    }
    public Walls(int x, int y, int width, int length,boolean destructable) {
        this.x = x;
        this.y = y;
        this.length = length;
        this.width = width;
        this.destructable=destructable;
    }




    //Getters


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public boolean isDestructable() {
        return destructable;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }
}
