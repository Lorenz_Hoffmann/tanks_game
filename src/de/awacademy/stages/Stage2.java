package de.awacademy.stages;

import de.awacademy.objectsgiven.MovingTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class Stage2 extends Stages{

    public Stage2(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){

        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));

        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));

        getEnemytanks().add(new MovingTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25-20)));

    }


}
