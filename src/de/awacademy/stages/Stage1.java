package de.awacademy.stages;

import de.awacademy.objectsgiven.*;

import java.util.ArrayList;

public class Stage1 extends Stages {


    public Stage1(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.4),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.2),(int) (getLengthofBoard()*0.1),true));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1)));

        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.8),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));


        getEnemytanks().add(new NotMovingTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25-20)));
    }




}
