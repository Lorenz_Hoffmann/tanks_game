package de.awacademy.stages;

import de.awacademy.objectsgiven.FastTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class Stage5 extends Stages{

    public Stage5(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){
        getWalls().add(new Walls((int)(getWidthofBoard()*0.2),(int) (getLengthofBoard()*0.75),(int) (getWidthofBoard()*0.25),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.55),(int) (getLengthofBoard()*0.75),(int) (getWidthofBoard()*0.25),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.45),(int) (getLengthofBoard()*0.75),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.05),true));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.35),(int) (getLengthofBoard()*0),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.5)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.65),(int) (getLengthofBoard()*0),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.5)));
        getEnemytanks().add(new FastTank((int)(getWidthofBoard()*0.25-20),(int) (getLengthofBoard()*0.45-20)));
        getEnemytanks().add(new FastTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25-20)));
        getEnemytanks().add(new FastTank((int)(getWidthofBoard()*0.75-20),(int) (getLengthofBoard()*0.35-20)));
    }
}
