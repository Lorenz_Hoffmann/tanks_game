package de.awacademy.stages;

import de.awacademy.objectsgiven.Tanks;
import de.awacademy.objectsgiven.Walls;

import javax.swing.text.TabableView;
import java.util.ArrayList;

public class Stages {

    private ArrayList<Walls> walls;
    private ArrayList<Tanks> enemytanks;


    private double lengthofBoard;
    private double widthofBoard;

    public Stages() {
    }

    public Stages(ArrayList<Walls> walls, ArrayList<Tanks> enemytanks) {
        this.walls = walls;
        this.enemytanks = enemytanks;
    }

    public Stages( double widthofBoard,double lengthofBoard) {

        this.lengthofBoard = lengthofBoard;
        this.widthofBoard = widthofBoard;
    }

    public void createStage(){}

    public ArrayList<Walls> getWalls() {
        return walls;
    }



    public void setWalls(ArrayList<Walls> walls) {
        this.walls = walls;
    }

    public ArrayList<Tanks> getEnemytanks() {
        return enemytanks;
    }

    public void setEnemytanks(ArrayList<Tanks> enemytanks) {
        this.enemytanks = enemytanks;
    }


    public double getLengthofBoard() {
        return lengthofBoard;
    }

    public double getWidthofBoard() {
        return widthofBoard;
    }
}
