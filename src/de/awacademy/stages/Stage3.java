package de.awacademy.stages;

import de.awacademy.objectsgiven.FastTank;
import de.awacademy.objectsgiven.MovingTank;
import de.awacademy.objectsgiven.NotMovingTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class Stage3 extends Stages{
    public Stage3(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){

        getWalls().add(new Walls((int)(getWidthofBoard()*0.35),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.55),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.15),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.75),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.35)));

        getWalls().add(new Walls((int)(getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.45),(int) (getLengthofBoard()*0.05)));

        getEnemytanks().add(new NotMovingTank((int)(getWidthofBoard()*0.25),(int) (getLengthofBoard()*0.1)));
        getEnemytanks().add(new MovingTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25-20)));
        getEnemytanks().add(new NotMovingTank((int)(getWidthofBoard()*0.75),(int) (getLengthofBoard()*0.1)));
    }
}
