package de.awacademy.stages;

import de.awacademy.objectsgiven.FastTank;
import de.awacademy.objectsgiven.GhostTank;
import de.awacademy.objectsgiven.NotMovingTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class Stage6 extends Stages{
    public Stage6(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.4),(int) (getLengthofBoard()*0.475),(int) (getWidthofBoard()*0.2),(int) (getLengthofBoard()*0.05),true));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.35),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));

        getWalls().add(new Walls((int)(getWidthofBoard()*0.35),(int) (getLengthofBoard()*0.75),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.3),true));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.75),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.3),true));

        getEnemytanks().add(new FastTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25)));
        getEnemytanks().add(new GhostTank((int)(getWidthofBoard()*0.25-20),(int) (getLengthofBoard()*0.5)));
        getEnemytanks().add(new GhostTank((int)(getWidthofBoard()*0.75-20),(int) (getLengthofBoard()*0.5)));
        getEnemytanks().add(new NotMovingTank((int)(getWidthofBoard()*0.1-20),(int) (getLengthofBoard()*0.9)));
        getEnemytanks().add(new NotMovingTank((int)(getWidthofBoard()*0.9-20),(int) (getLengthofBoard()*0.9)));
    }
}
