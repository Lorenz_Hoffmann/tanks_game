package de.awacademy.stages;

import de.awacademy.objectsgiven.GhostTank;
import de.awacademy.objectsgiven.NotMovingTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class Stage4 extends Stages{
    public Stage4(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());

        createStage();
    }
    @Override
    public void createStage(){
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.4),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.2),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.7),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.6),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.3),(int) (getLengthofBoard()*0.05)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.85),(int) (getLengthofBoard()*0.35),(int) (getWidthofBoard()*0.05),(int) (getLengthofBoard()*0.35)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.4),(int) (getLengthofBoard()*0.3),(int) (getWidthofBoard()*0.2),(int) (getLengthofBoard()*0.05),true));

        getEnemytanks().add(new GhostTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.5-20)));
    }
}
