package de.awacademy.stages;

import de.awacademy.objectsgiven.DummyTank;
import de.awacademy.objectsgiven.Walls;

import java.util.ArrayList;

public class WinningStage extends Stages {
    public WinningStage(double widthofBoard,double lengthofBoard) {
        super(widthofBoard, lengthofBoard);
        setWalls(new ArrayList<>());
        setEnemytanks(new ArrayList<>());
        createStage();

    }
    @Override
    public void createStage(){
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.8),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.8),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.8),(int) (getLengthofBoard()*0.8),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.45),(int) (getLengthofBoard()*0.1),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.45),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getWalls().add(new Walls((int)(getWidthofBoard()*0.8),(int) (getLengthofBoard()*0.45),(int) (getWidthofBoard()*0.1),(int) (getLengthofBoard()*0.1)));
        getEnemytanks().add(new DummyTank((int)(getWidthofBoard()*0.5-20),(int) (getLengthofBoard()*0.25-20)));
    }
}
