package de.awacademy;

import de.awacademy.model.Model;
import de.awacademy.objectsgiven.*;
import de.awacademy.stages.GameOver;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;
import org.w3c.dom.css.Rect;

import java.io.FileInputStream;
import java.util.Collections;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    Image greenBarrel = new Image("file:\\Users\\Academy\\IdeaProjects\\Woche4\\gameTanks\\src\\de\\awacademy\\images\\Barrel2.png");
    Image redBarrel = new Image("file:\\Users\\Academy\\IdeaProjects\\Woche4\\gameTanks\\src\\de\\awacademy\\images\\BarrelRed2.png");
    Image blueBarrel = new Image("file:\\Users\\Academy\\IdeaProjects\\Woche4\\gameTanks\\src\\de\\awacademy\\images\\BarrelBlue2.png");
    Image greyBarrel = new Image("file:\\Users\\Academy\\IdeaProjects\\Woche4\\gameTanks\\src\\de\\awacademy\\images\\BarrelGrey2.png");
    Image yellowBarrel = new Image("file:\\Users\\Academy\\IdeaProjects\\Woche4\\gameTanks\\src\\de\\awacademy\\images\\BarrelYellow2.png");
    public Graphics (GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }


    public void draw() {

        gc.fill();
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);



        //gc.fillText("Score " + model.getPointCounter(),20,20);

        if(model.isGameOver()){
            gc.setFill(Color.DARKRED);
            gc.setFont(Font.font(50));
            gc.fillText("Game Over " ,300,400);
            gc.setFill(Color.BLACK);
            gc.setFont(Font.font(30));
            gc.fillText("Shoot tank to restart" ,285,450);
        }

        if(model.isWin()){
            gc.setFill(Color.GOLD);
            gc.setFont(Font.font(50));
            gc.fillText("You Won" ,300,400);
            gc.setFill(Color.BLACK);
            gc.setFont(Font.font(30));
            gc.fillText("Shoot tank to restart" ,285,450);
        }


        for (Tanks tank: model.getEnemyTanks()) {
            drawPanzer(tank);

        }
        gc.setFill(Color.DARKSALMON);

        for (Walls wall: model.getGameBorderWalls()) {
            gc.fillRect(wall.getX(),wall.getY(),wall.getWidth(),wall.getLength());
        }



        for (Walls wall: model.getOtherwalls()) {
            if(!wall.isDestructable()) {
                gc.setFill(Color.BROWN);
                gc.fillRoundRect(wall.getX(), wall.getY(), wall.getWidth(), wall.getLength(), 5, 5);
                gc.setFill(Color.BURLYWOOD);
                if (wall.getWidth() <= wall.getLength()) {
                    gc.fillRoundRect(wall.getX() + 0.1 * wall.getWidth(), wall.getY() + 0.1 * wall.getWidth(), wall.getWidth() - 0.2 * wall.getWidth(), wall.getLength() - 0.2 * wall.getWidth(), 5, 5);
                } else {
                    gc.fillRoundRect(wall.getX() + 0.1 * wall.getLength(), wall.getY() + 0.1 * wall.getLength(), wall.getWidth() - 0.2 * wall.getLength(), wall.getLength() - 0.2 * wall.getLength(), 5, 5);
                }
            }else{
                for (int i = 0; i <10 ; i++) {
                    for (int j = 0; j <10 ; j++) {
                        double newx = wall.getWidth()*i/10+wall.getX();
                        double newy = wall.getLength()*j/10+wall.getY();
                        double newwidth = wall.getWidth()*1/10;
                        double newlength = wall.getLength()*1/10;
                        gc.setFill(Color.BROWN);
                        gc.fillRect(newx, newy, newwidth, newlength);
                        gc.setFill(Color.BURLYWOOD);
                        if (wall.getWidth() <= wall.getLength()) {
                            gc.fillRoundRect(newx + 0.1 * newwidth, newy + 0.1 * newlength, newwidth - 0.2 * newwidth, newlength - 0.2 * newwidth, 5, 5);
                        } else {
                            gc.fillRoundRect(newx + 0.1 * newwidth, newy + 0.1 * newlength, newwidth - 0.2 * newlength, newlength - 0.2 * newlength, 5, 5);
                        }
                    }
                }
            }
        }



        for (Bullets bullet: model.getBulletsinGame()){
            if(bullet.isIngame()) {
                if(bullet instanceof NormalBullet){
                    gc.setFill(Color.DARKGREY);
                }else if(bullet instanceof BouncingBullet) {
                    gc.setFill(Color.BLACK);
                }else if(bullet instanceof FastBullet) {
                    gc.setFill(Color.DARKRED);
                }else if(bullet instanceof GhostBullet) {
                    gc.setFill(Color.DARKSLATEGREY);
                }
                gc.fillOval(bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getLength());
            }
        }


        double x = model.getPlayer().getShootingBarrel().getX()-(model.getPlayer().getShootingBarrel().getWidthfordrawing()+10)/2;
        double y = model.getPlayer().getShootingBarrel().getY();
        double width = model.getPlayer().getShootingBarrel().getWidthfordrawing()+10;
        double height = model.getPlayer().getShootingBarrel().getLengthfordrawing();

        double rotationCenterX = model.getPlayer().getShootingBarrel().getX();
        double rotationCenterY = model.getPlayer().getShootingBarrel().getY();

        
        gc.setFill(Color.DARKGREEN);
        gc.save();
        gc.transform(new Affine(new Rotate(model.getPlayer().getShootingBarrel().getAngleToCenter(), rotationCenterX,rotationCenterY)));
        //gc.fillRect(x, y, width, height);
        //gc.setFill(Color.TRANSPARENT);
        gc.drawImage(blueBarrel,x, y, width, height);
        gc.restore();
        gc.setFill(Color.BLUE);
        gc.fillRoundRect(model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getWidth(), model.getPlayer().getLength(),5,5
        );
        gc.setFill(Color.DARKBLUE);
        gc.fillRoundRect(model.getPlayer().getX()+3, model.getPlayer().getY()+3,
                model.getPlayer().getWidth()-6, model.getPlayer().getLength()-6,5,5
        );
        gc.setFill(Color.LIGHTBLUE);
        gc.fillRoundRect(model.getPlayer().getX()+10, model.getPlayer().getY()+10,
                model.getPlayer().getWidth()-20, model.getPlayer().getLength()-20,5,5
        );




    }

    public void drawPanzer(Tanks tank){

        double x = tank.getShootingBarrel().getX()-(tank.getShootingBarrel().getWidthfordrawing()+10)/2;
        double y = tank.getShootingBarrel().getY();
        double width = tank.getShootingBarrel().getWidthfordrawing()+10;
        double height = tank.getShootingBarrel().getLengthfordrawing();

        double rotationCenterX =tank.getShootingBarrel().getX();
        double rotationCenterY = tank.getShootingBarrel().getY();


        if(tank instanceof FastTank) {

            gc.save();
            gc.transform(new Affine(new Rotate(tank.getShootingBarrel().getAngleToCenter(), rotationCenterX,rotationCenterY)));
            gc.drawImage(redBarrel,x, y, width, height);
            //gc.fillRect(x, y, width, height);
            gc.restore();

            gc.setFill(Color.RED);
            gc.fillRoundRect(tank.getX(), tank.getY(), tank.getWidth(), tank.getLength(), 5, 5);

            gc.setFill(Color.DARKRED);
            gc.fillRoundRect(tank.getX() + 3, tank.getY() + 3,
                    tank.getWidth() - 6, tank.getLength() - 6, 5, 5
            );
            gc.setFill(Color.LIGHTGOLDENRODYELLOW);
            gc.fillRoundRect(tank.getX() + 10, tank.getY() + 10,
                    tank.getWidth() - 20, tank.getLength() - 20, 5, 5
            );
        }else if(tank instanceof MovingTank) {

            gc.save();
            gc.transform(new Affine(new Rotate(tank.getShootingBarrel().getAngleToCenter(), rotationCenterX,rotationCenterY)));
            gc.drawImage(greenBarrel,x, y, width, height);
            //gc.fillRect(x, y, width, height);
            gc.restore();

            gc.setFill(Color.GREEN);
            gc.fillRoundRect(tank.getX(), tank.getY(), tank.getWidth(), tank.getLength(), 5, 5);

            gc.setFill(Color.DARKGREEN);
            gc.fillRoundRect(tank.getX() + 3, tank.getY() + 3,
                    tank.getWidth() - 6, tank.getLength() - 6, 5, 5
            );
            gc.setFill(Color.LIGHTGREEN);
            gc.fillRoundRect(tank.getX() + 10, tank.getY() + 10,
                    tank.getWidth() - 20, tank.getLength() - 20, 5, 5
            );
        }else if(tank instanceof GhostTank) {

            gc.save();
            gc.transform(new Affine(new Rotate(tank.getShootingBarrel().getAngleToCenter(), rotationCenterX,rotationCenterY)));
            gc.drawImage(greyBarrel,x, y, width, height);
            //gc.fillRect(x, y, width, height);
            gc.restore();

            gc.setFill(Color.GREY);
            gc.fillRoundRect(tank.getX(), tank.getY(), tank.getWidth(), tank.getLength(), 5, 5);

            gc.setFill(Color.BLACK);
            gc.fillRoundRect(tank.getX() + 3, tank.getY() + 3,
                    tank.getWidth() - 6, tank.getLength() - 6, 5, 5
            );
            gc.setFill(Color.LIGHTGREY);
            gc.fillRoundRect(tank.getX() + 10, tank.getY() + 10,
                    tank.getWidth() - 20, tank.getLength() - 20, 5, 5
            );
        }else if(tank instanceof NotMovingTank|| tank instanceof DummyTank) {


            gc.save();
            gc.transform(new Affine(new Rotate(tank.getShootingBarrel().getAngleToCenter(), rotationCenterX, rotationCenterY)));
            gc.drawImage(yellowBarrel, x, y, width, height);
            //gc.fillRect(x, y, width, height);
            gc.restore();


            gc.setFill(Color.YELLOW);
            gc.fillRoundRect(tank.getX(), tank.getY(), tank.getWidth(), tank.getLength(), 5, 5);

            gc.setFill(Color.DARKORANGE);
            gc.fillRoundRect(tank.getX() + 3, tank.getY() + 3,
                    tank.getWidth() - 6, tank.getLength() - 6, 5, 5
            );
            gc.setFill(Color.LIGHTYELLOW);
            gc.fillRoundRect(tank.getX() + 10, tank.getY() + 10,
                    tank.getWidth() - 20, tank.getLength() - 20, 5, 5
            );
        }

    }


}
