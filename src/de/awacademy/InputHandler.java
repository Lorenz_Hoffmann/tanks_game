package de.awacademy;

import de.awacademy.model.Model;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;

public class InputHandler {

    private Model model;

    public InputHandler (Model model) {
        this.model = model;

    }



    public void onKeyPressed(KeyCode keycode) {

        if(keycode == KeyCode.W&&!model.isoccupied(model.getPlayer(),0,-6)) {
            model.getPlayer().move(0, -6);
        }
        if(keycode == KeyCode.S&&!model.isoccupied(model.getPlayer(),0,6)) {
            model.getPlayer().move(0, 6);
        }
        if(keycode == KeyCode.A&&!model.isoccupied(model.getPlayer(),-6,0)) {
            model.getPlayer().move(-6, 0);
        }
        if(keycode == KeyCode.D&&!model.isoccupied(model.getPlayer(),6,0)) {
            model.getPlayer().move(6, 0);
        }

        if(keycode == KeyCode.E&&!model.isoccupied(model.getPlayer(),3,-3)) {
            model.getPlayer().move(3, -3);
        }
        if(keycode == KeyCode.Q&&!model.isoccupied(model.getPlayer(),-3,-3)) {
            model.getPlayer().move(-3, -3);
        }
        if(keycode == KeyCode.C&&!model.isoccupied(model.getPlayer(),3,3)) {
            model.getPlayer().move(3, 3);
        }
        if(keycode == KeyCode.Y&&!model.isoccupied(model.getPlayer(),-3,3)) {
            model.getPlayer().move(-3, 3);
        }

        if(keycode == KeyCode.P) {
            if(model.getStages().size()-1 > model.getStageindex() ) {
                model.makenewStage();
                model.setStageindex(model.getStageindex() + 1);

            }else{
                model.setStageindex(0);
            }

        }


        if(keycode == KeyCode.SPACE){
            model.shootbyPLayer(5);


        }

/*
        if(keycode == KeyCode.W) {
            model.getPlayer().getShootingBarrel().makemove(0, -1);
        }
        if(keycode == KeyCode.A) {
            model.getPlayer().getShootingBarrel().makemove(-1, 0);
        }
        if(keycode == KeyCode.D) {
            model.getPlayer().getShootingBarrel().makemove(1, 0);
        }
        if(keycode == KeyCode.S) {
            model.getPlayer().getShootingBarrel().makemove(0, 1);
        }

 */

    }
}
